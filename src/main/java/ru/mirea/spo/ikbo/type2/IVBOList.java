package ru.mirea.spo.ikbo.type2;

public interface IVBOList {

    int add(int elementValue);

    int get(int elementIndex);

    void set(int elementIndex, int elementValue);

    void remove(int elementIndex);

}

package ru.mirea.spo.ikbo.type2;

public class IBVOLinkedList implements IVBOList {

    private Item first = null;

    //TODO
    private Item last = null;

    @Override
    public int add(int elementValue) {

        Item item = new Item(elementValue);

        if (first == null) {
            item.setIndex(0);
            first = item;
        } else {

            Item element = first;

            while (element.getNext() != null) {
                element = element.getNext();
            }

            element.setNext(item);
            item.setIndex(element.getIndex() + 1);

        }

        return item.getIndex();
    }

    @Override
    public int get(int elementIndex) {
        Item element = first;

        while (element.getIndex() != elementIndex) {
            element = element.getNext();
        }

        return element.getValue();
    }

    @Override
    public void set(int elementIndex, int elementValue) {

    }

    @Override
    public void remove(int elementIndex) {

    }

    public static void main(String[] args) {
        IVBOList list = new IBVOLinkedList();

        list.add(1);
        list.add(7);
        boolean result = list.get(0) == 1;
        System.out.println("LIST get 1: " + list.get(1));
        System.out.println("TRUE: " + result);

    }

}

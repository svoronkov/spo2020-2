package ru.mirea.spo.ikbo.type2;

public class Item {

    private int value;

    private Item next;

    private int index;

    public Item(int value) {
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Item getNext() {
        return next;
    }

    public void setNext(Item next) {
        this.next = next;
    }

    public int getValue() {
        return value;
    }
}

package ru.mirea.spo.ikbo.lexer;

import ru.mirea.spo.ikbo.token.Token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;

public class Lexer {

    private final String rawInput;

    public Lexer(String rawInput) {
        this.rawInput = rawInput;
    }

    public String getRawInput() {
        return rawInput;
    }

    public List<Token> tokens() {
        String source = rawInput;
        int currentIndex = 0;
        int currentIndexFrom = 0;

        boolean okWaiting = true;
        List<Token> tokens = new ArrayList<>();
        List<Lexem> prevLexems = null;

        while (currentIndex < source.length()) {
            String acc = source.substring(currentIndexFrom, currentIndex + 1);
            List<Lexem> currentLexems = new ArrayList<>();

            Lexem currentLexem = null;

            for (Lexem lexem : Lexem.values()) {
                Matcher matcher = lexem.getPattern().matcher(acc);
                if (matcher.find()) {
                    //TODO добавлять в список кондидатов на успешную лексему по-хорошему
                    // currentLexems.add(lexem);

                    currentLexem = lexem;
                }
            }
            if (currentLexems != null) {
                prevLexems = currentLexems;
            }
            if (okWaiting && currentLexems.size() > 0) {
                okWaiting = false;
            }
            if (okWaiting && currentLexems.size() == 0) {
                throw new RuntimeException("Incorrect source");
            }
            if (!okWaiting && currentLexems.size() == 0) {
                // убедиться, что ваша лексема не перекрывается лексемой ключевого слова
                // (то есть лексемой более высокого приоритета)
                Lexem lexem = getHighestPriority(prevLexems);
                Token token = new Token(lexem, acc.substring(0, acc.length() - 1));
                tokens.add(token);
                System.out.println("TOKEN:" + token);
                okWaiting = true;
                currentIndexFrom = currentIndex;
            } else {
                currentIndex = currentIndex + 1;
            }
        }
        return tokens;
    }

    /**
     * TODO реализовать метод поиска в списке лексем
     * такой лексемы, у которой самый высокий приоритет
     * -- private int priority; --
     */
    private Lexem getHighestPriority(List<Lexem> prevLexems) {
        return null;
    }

    public static void main(String[] args) {
        Lexer lexer = new Lexer("cat=1+450+500+1000+20-40");
        for (Token token: lexer.tokens()) {
            System.out.println(token);
        }
    }
}

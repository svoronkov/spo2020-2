package ru.mirea.spo.ikbo.lexer;


import java.util.regex.Pattern;

/**
 * VAR -> [a-z]+
 * DIGIT -> 0|([1-9][0-9]*)
 * ASSIGN_OP -> =
 * OP -> +|-|*|/
 */

public enum Lexem {

    /** TODO: ввести приоритет
     *
     */

    VAR("^[a-zA-Z]+$", 0),
    DIGIT("^(0|([1-9][0-9]*))$", 0),
    ASSIGN_OP("^=$", 0),
    OP("^(\\+|-|\\*|/)$", 0),
    // TODO LIST_OP : ADD, GET, REMOVE, SET

    WHILE_KW("^while$", 1),
    FOR_KW("^for$", 1),
    HASH_SET_KW("^HASH_SET$", 1);
    // TODO LIST_TYPE_KW

    private final Pattern pattern;
    private int priority;

    Lexem(String regexp, int priority) {
        this.pattern = Pattern.compile(regexp);
        this.priority = priority;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public int getPriority() {
        return priority;
    }
}

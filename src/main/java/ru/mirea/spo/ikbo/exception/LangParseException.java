package ru.mirea.spo.ikbo.exception;

public class LangParseException extends Throwable {
    public LangParseException(String message) {
        super(message);
    }
}

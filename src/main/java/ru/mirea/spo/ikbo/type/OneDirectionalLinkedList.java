package ru.mirea.spo.ikbo.type;

public class OneDirectionalLinkedList implements BaseList {

    /**
     * TODO варианты оптимизации:
     * 1) Хранить ссылку на последний вставленный элемент
     * 2) Ссылаться на элементы отрезков (середина, четверть и т.д.)
     */

    private int totalCount = 0;

    private Node first;
    private Node last;

    @Override
    public Node get(int index) {
        return null;
    }

    public Node getFirst() {
        return first;
    }

    @Override
    public Node getLast() {
        return null;
    }

    public Node add(Object value) {
        // TODO бежать по всем значениям next от first пока next не вернёт null
        Node node = new Node(value, null);
        if (totalCount == 0) {
            first = node;
            last = node;
        } else {
            last.setNext(node);
        }
        return node;
    }

}

package ru.mirea.spo.ikbo.type;

public interface BaseList {

    Object get(int index);

    Object getFirst();

    Object getLast();

}

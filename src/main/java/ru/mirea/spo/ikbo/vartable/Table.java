package ru.mirea.spo.ikbo.vartable;

import java.util.HashMap;
import java.util.Map;

public class Table {

    // Строго в однопоточном режиме
    // Область видимости -> локальная таблица переменных
    Map<String, Map<String, Integer>> variables = new HashMap<>();

    void putVariable(String name, Integer value) {
        //TODO позволить методу работать с областями видимости
        //variables.put(name, value);
    }

    Integer getVariable(String name, String scope) {
        //TODO позволить методу работать с областями видимости
        //return variables.get(name);
        return null;
    }
}

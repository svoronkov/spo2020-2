package ru.mirea.spo.ikbo;

import ru.mirea.spo.ikbo.exception.LangParseException;
import ru.mirea.spo.ikbo.lexer.Lexer;
import ru.mirea.spo.ikbo.parser.Parser;
import ru.mirea.spo.ikbo.token.Token;

import java.util.List;

public class UILang {

    public static void main(String[] args) throws LangParseException {
        System.out.println("Start ...");

        // аргумен args[1] - это путь к файлу,
        // где лежит код на языке Lang
        Lexer lexer = new Lexer("catAge=1+30");

        List<Token> tokens = lexer.tokens();

        for (Token token: tokens) {
            System.out.println(token);
        }

        Parser parser = new Parser( tokens );

        parser.lang();

    }

}
